# Installing the documentation
    
## How to install the documentation on your workstation

### Cloning the repository

In the terminal:

SSH:
`git clone git@gitlab.com:sat-metalab/documentations/satie.git`

HTTPS:
`git clone https://gitlab.com/sat-metalab/documentations/satie.git`

And then, navigate to the `satie` repo:

`cd satie`

### Requirements

Check the requirements.txt file for a list of requirements.

You should install them in a virtual environment.

First start by creating a virtual environment:

    cd satie-docs
    python3 -m virtualenv venv
	
On Ubuntu, you can activate the environment with:

    source venv/bin/activate

You can then install the requirements with:

    pip install wheel
    pip install -r requirements.txt

## Building the documentation

To build the English documentation in the `build/html/locales/en` folder:

`sphinx-build -b html -D language=en source build/html/locales/en`

To build the French documentation in the `build/html/locales/fr` folder:

`sphinx-build -b html -D language=fr source build/html/locales/fr`

## Serving the documentation

In the `satie` repo, execute the following line in the Terminal:

`python3 -m http.server`

The website should be live at `http://0.0.0.0:8000/`

To access the main page of the website, you may need to navigate to the right locale:

For the English version: `http://0.0.0.0:8000/build/html/locales/en/`

For the French version: `http://0.0.0.0:8000/build/html/locales/fr/`

## Contributing to the documentation

See `CONTRIBUTING.md`.
    

