# Contributing to the documentation

Now that you have installed the documentation, you are ready to contribute to the project.

## Ressources

All of Metalab documentations websites are based on the few following repos:

- the [template-website](https://gitlab.com/sat-mtl/documentation/template-website) for the web dev contents
- the [intro](https://gitlab.com/sat-mtl/metalab/documentations/intro) for general information on how to contribute to documentation
- the [templates](https://gitlab.com/sat-mtl/metalab/documentations/templates) for documentation templates

## Read the Code of Conduct

Please start by reading the Code of Conduct of this projet: `CODE_OF_CONDUCT.md`.

## Overview of the repo structure

The repo is structured in the following way:

    - build
        - doctrees
        - gettext
        - html
            - locales
                - en
                - fr
    - source
        - api
        - blog
        - faq
        - glossary
        - howto
        - locales
        - _static
        - _templates
        - tutorials
        conf.py
        index.rst
        temp.py

    - venv
    .gitignore
    .gitlab-ci.yml
    .temp
    CONTRIBUTING.md
    INSTALL.md
    make.bat
    Makefile
    README.md

## How the docs are build

The folders in the `source` folder contains all the original documents.

The `source/locales` folder contains the original documents translated in a specific locale, as `.po` files.

## Create a new branch

In the documentations folder on your computer, enter the following:

	cd satie
	git checkout -b feature/BRANCH-NAME

## Updating the documentation

Locate the file you want to update in the `source` folder.

Make the changes you wish to that file, in the original `.rst` format.

To generate the `source/locales` files in the `.po` format, run the following commands in the Terminal:

`make intl`

Do the English to French translations directly in the `.po` files located in the `source/locales/fr` folders.

## Building the new documentation

To build the new documentation:

`make htmllocaltest`

Which will build both English and French documentation.

## Review your changes

Using the Python standard library, we will serve locally the new documentation in order to review the changes.

In the `satie` folder, run in the Terminal:

`python3 -m http.server`

Next, navigate to `http://0.0.0.0:8000/` and the landing page (English by default) will let you choose your language.

## Making a merge request

After reviewing your changes and making sure that the suggested updates works correctly, you may submit a merge request.

Note that your merge request should have the `dev` branch as the target.
