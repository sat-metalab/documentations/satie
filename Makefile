# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = source
BUILDDIR      = public
PROJECT	      = satie

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Make the documentation
doc:
	@$(SPHINXBUILD) -b html -D language=en source $(BUILDDIR)/en
	@$(SPHINXBUILD) -b html -D language=fr source $(BUILDDIR)/fr
	cp source/index.html $(BUILDDIR)/

intl:
	make gettext
	sphinx-intl update -p $(BUILDDIR)/gettext -l fr

htmlgitlabdocs:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./documentation/$(PROJECT)/' source $(BUILDDIR)/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./documentation/$(PROJECT)/' source $(BUILDDIR)/fr
	cp source/index.html $(BUILDDIR)/

htmllocaltest:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./$(BUILDDIR)/' source $(BUILDDIR)/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./$(BUILDDIR)/' source $(BUILDDIR)/fr
	cp source/index.html $(BUILDDIR)/

htmlgitlab:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./$(PROJECT)/' source $(BUILDDIR)/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./$(PROJECT)/' source $(BUILDDIR)/fr
	cp source/index.html public/

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
