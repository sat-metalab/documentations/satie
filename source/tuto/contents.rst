.. _tutorials:

=====================
Tutorials
=====================
   
If you are just beginning with SATIE, please take a look at :doc:`configure <configure>` first. :doc:`cheatsheet <cheatsheet>` is a good next step, as is :doc:`basics <basics>`.

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: Available tutorials:

   quickstart
   basics
   configure
   ambisonics
   effects
   nonrealtime
   plugins
   processes
   spatializers
   cheatsheet

