SATIE quickstart guide
======================

This guide presents a minimal setup for SATIE. Use it to ensure that your SATIE installation is working correctly.

Prerequisites
-------------

* You have installed SuperCollider and sc3-plugins.
* You have installed the SATIE Quark for SuperCollider. (See :doc:`Installing SATIE <../installation/contents>`.)
* You have a working stereo audio output (headphones, speakers, or earphones).
* You are using the SuperCollider IDE (scide).

Boot the server
---------------

Copy the following lines of code into a new file in the IDE. Run the code to boot the SATIE server.

.. code-block:: supercollider

   (
   // Change the server from scsynth to supernova.
   s = Server.supernova.local;
   // Generate a SATIE configuration using a stereo spatializer.
   ~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
   // Use the configuration to generate a Satie object.
   ~satie = Satie.new(~satieConfiguration);
   // Get the SATIE OSC address.
   ~addr = ~satie.osc.oscServer;
   // Boot the SATIE server.
   ~addr.sendMsg('/satie/boot');
   )

.. note::

	To run code in the SuperCollider IDE, select the lines and press :kbd:`Ctrl` + :kbd:`Enter`. Aternatively, press :kbd:`Ctrl` + :kbd:`Enter` while the cursor is between a pair of outside parentheses to run all the code enclosed between them.

Look for the following output in the post window to ensure that the server boots correctly.

.. code-block::

   - satie: a Satie
   - rootURI: /satie
   - port: 18032
   + localhost

Create an audio source
----------------------

Next, create an audio source by sending SATIE an OSC message from SuperCollider. Use `dustyRez <https://gitlab.com/sat-mtl/tools/satie/satie/-/blob/develop/plugins/sources/dustyRez.scd>`__, an audio plugin built into SATIE that produces the sound of `dust <https://doc.sccode.org/Classes/Dust.html>`__ going through resonators. To do this, run the following code in the IDE.

.. code-block:: supercollider

   ~addr.sendMsg('/satie/scene/createSource', 'mySource', 'dustyRez');

Confirm that the code has executed correctly by looking for the following output in the post window.

.. code-block:: supercollider

   -> a NetAddr(127.0.0.1, 18032)

.. tip::

	In the next step, SATIE will produce sound. It is best to stop sound using the clear message at the end of this guide. In an emergency, you can stop all SuperCollider sound processes from the IDE by pressing :kbd:`Ctrl` + :kbd:`.` at any time. If you do so, you will have to restart this guide from the first code block.

Activate the audio source
-------------------------

By default, the gain of the audio source is set to -99 dB. To make the source produce sound, set its gain to an audible level by running the following code.

.. code-block:: supercollider

   // Adjust the gainDB value as needed.
   ~addr.sendMsg('/satie/source/set', 'mySource', 'gainDB', -20);

If this line runs correctly, then the interpreter will respond in the post window the same way it did in the previous step, and the audio output will emit audio that sounds like wind chimes. Here is a sample.

.. raw:: html

    <audio controls="controls">
      <source src="../_static/dustyrezsamp.mp3" type="audio/mpeg">
      Your browser does not support the <code>audio</code> element. 
    </audio>

Bonus: A first spatialization (optional)
----------------------------------------

To showcase audio spatialization, change the rotational position of the source by setting its azimuth. The following code rotates the audio source clockwise in 30-degree steps until it is directly to the right of the listener.

.. code-block:: supercollider

   (
   Routine {
   ~addr.sendMsg('/satie/source/set', 'mySource', 'aziDeg', 30);
   3.wait;
   ~addr.sendMsg('/satie/source/set', 'mySource', 'aziDeg', 60);
   3.wait;
   ~addr.sendMsg('/satie/source/set', 'mySource', 'aziDeg', 90);
   }.play;
   )

Here is a sample of the audio that should result.

.. raw:: html

    <audio controls="controls">
      <source src="../_static/dustyrezambi.mp3" type="audio/mpeg">
      Your browser does not support the <code>audio</code> element. 
    </audio>

Stop the Server
---------------

To properly stop the audio, send the clear scene message to the server.

.. code-block:: supercollider

   ~addr.sendMsg('/satie/scene/clear');

To shut down the server, send the quit message.

.. code-block:: supercollider

   ~addr.sendMsg('/satie/quit');
