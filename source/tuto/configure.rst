.. _configure:

Configure and boot SATIE
========================

First, we create a :code:`SatieConfiguration`, then, we create a :code:`Satie` with this configuration and boot it.

:code:`SatieConfiguration` needs to be given a :code:`Server` and at least one :code:`listeningFormat`. Here, we give it the default Server and a stereo spatializer.

.. code-block:: supercollider

    ~config = SatieConfiguration(s, [\stereoListener]);


SatieConfiguration should have set its server's number of output channels. :code:`stereoListener` has 2 output channels, therefore the server should have 2 outputs.

.. code-block:: supercollider

    ~config.server.options.numOutputBusChannels


If needed, other server options can be set through SatieConfiguration.

.. code-block:: supercollider

    ~config.server.options.numWireBufs = 2048;
    ~config.server.options.memSize = 1024 * 256;


SatieConfiguration scans SATIE's plugin folders and collects the plugins in its dictionaries.

.. code-block:: supercollider

    ~config.sources;
    ~config.effects;
    ~config.spatializers;
    ~config.hoa;
    ~config.mappers;
    ~config.postprocessors;
    ~config.monitoring;


An instance of SATIE can be created using this configuration. During boot, SynthDefs for all of its plugins will be generated.
Once booted, SATIE will start listening to incoming OSC messages.

.. code-block:: supercollider

    ~satie = Satie(~config);
    ~satie.boot;


SATIE can be stopped using the 'quit' method. This will clean up after itself.

.. code-block:: supercollider

    ~satie.quit;
